﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Customer> GetCustomerWithDataAsync(Guid Id);
        Task CreateCustomerWithPreferencesAsync(Customer customer, List<Guid> preferenceIds);
        Task EditCustomerWithPreferencesAsync(Customer customer, List<Guid> preferenceIds);
    }
}
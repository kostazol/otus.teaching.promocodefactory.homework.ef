﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface IDbInitializer
    {
        void Init();
    }
}
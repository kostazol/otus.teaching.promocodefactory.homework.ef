﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<Customer> customerRepository,
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var allPromoCodes = await _promoCodeRepository.GetAllAsync();
            var promocodesModelList = allPromoCodes.Select(x => new PromoCodeShortResponse()
            {
                Code = x.Code,
                Id = x.Id,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();
            return promocodesModelList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            StatusCodeResult result = Ok();
            try
            {
                // не правильные api preference должны передаваться как id. Либо надо ставить уникальный ключ на имя preference.
                // не правильные api preference beginDate, endDate обязательно должны передаваться.
                // не правильные api pregerence должна передаваться информация о том, кто создает
                // var prenerences = await _preferenceRepository.GetAllAsync(x => x.Name == request.Preference);
                // var preference = prenerences.FirstOrDefault(); // берем первый ппопавшийся с подходящим именем.
                // var customers = await _customerRepository.GetAllAsync(x =>
                //     x.CustomerPreferences.Exists(y => y.Preference.Name == request.Preference));
                // var oneCustomer = customers.FirstOrDefault();
                // //todo: поставить обработчик, мб и нет подходящих клиентов.
                //
                // var promoCode = new PromoCode()
                // {
                //     Code = request.PromoCode,
                //     ServiceInfo = request.ServiceInfo,
                //     PartnerName = request.PartnerName,
                //     BeginDate = DateTime.Now, // делаем заглушку т.к. информация не передается 
                //     EndDate = DateTime.Now.AddDays(7), // делаем заглушку т.к. информация не передается 
                //     PreferenceId = preference.Id,
                //     PartnerManagerId =
                //         Guid.Parse(
                //             "53729686-a368-4eeb-8bfa-cc69b6050d02"), // так как информация не передается с реквеста, все промокоды создает админ
                //     // по п4 "Связь Customer и Promocode реализовать через One-To-Many, будем считать, что в данном примере промокод может быть выдан только одному клиенту из базы."
                //     // по п7 должен сохранять новый промокод в базе данных и находить клиентов с переданным предпочтением и добавлять им данный промокод
                //     // п7 противоричит п4. Можно найти только одно клиента с нужным предпочтением.
                //     CustomerId = oneCustomer.Id
                // };
            }
            catch (Exception e)
            {
                result = BadRequest();
            }

            return result;
        }
    }
}
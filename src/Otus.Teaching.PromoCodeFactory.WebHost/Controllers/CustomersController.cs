﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _prefernceRepository;

        public CustomersController(ICustomerRepository customerRepository, IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> prefernceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _prefernceRepository = prefernceRepository;
        }

        /// <summary>
        /// Получить всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            // Вопрос3: как лучше возвращать Task<ActionResult<List<CustomerShortResponse>>> или Task<List<CustomerShortResponse>>
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить данные покупателя по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetCustomerWithDataAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.FullName,
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList(),
                PreferenceResponses = customer.CustomerPreferences.Select(x => new PreferenceShortReponse()
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                }).ToList()
            };

            return customerModel;
        }

        /// <summary>
        /// Создать покупателя с его предпочтениями.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var customer = new Customer()
                {
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                };
                await _customerRepository.CreateCustomerWithPreferencesAsync(customer, request.PreferenceIds);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Измененить покупателя с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                
                if (customer == null)
                    return NotFound();
                
                customer.FirstName = request.FirstName;
                customer.LastName = request.LastName;
                customer.Email = request.Email;
                await _customerRepository.EditCustomerWithPreferencesAsync(customer, request.PreferenceIds);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                
                if (customer == null)
                    return NotFound();
                
                await _customerRepository.RemoveAsync(customer);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
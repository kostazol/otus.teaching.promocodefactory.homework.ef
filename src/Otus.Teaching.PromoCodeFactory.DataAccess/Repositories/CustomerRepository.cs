﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(EfContext context)
            : base(context)
        {
        }

        public async Task<Customer> GetCustomerWithDataAsync(Guid Id)
        {
            var customersWithDatas = _dbSet
                .Include(x => x.PromoCodes)
                .Include(x => x.CustomerPreferences)
                .ThenInclude(x => x.Preference);

            var customerWithPreferences = await customersWithDatas.FirstOrDefaultAsync(x => x.Id == Id);
            return customerWithPreferences;
        }

        public async Task CreateCustomerWithPreferencesAsync(Customer customer, List<Guid> preferenceIds)
        {
            var guid = Guid.NewGuid();
            var customerPreferences = _context.Preferences
                .Where(x => preferenceIds.Contains(x.Id))
                .Select(x => new CustomerPreference()
                {
                    CustomerId = guid,
                    PreferenceId = x.Id
                }).ToList();
            customer.CustomerPreferences = customerPreferences;
            await _dbSet.AddAsync(customer);
            throw new NotImplementedException();
        }

        public async Task EditCustomerWithPreferencesAsync(Customer customer, List<Guid> preferenceIds)
        {
            var customerPreferences = _context.Preferences
                .Where(x => preferenceIds.Contains(x.Id))
                .Select(x => new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id
                }).ToList();
            customer.CustomerPreferences = customerPreferences;
            _dbSet.Update(customer);
            await _context.SaveChangesAsync();
        }
    }
}
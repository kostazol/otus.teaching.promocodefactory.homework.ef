﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfConfigurations
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(p => p.Code).HasMaxLength(150);
            builder.Property(p => p.PartnerName).HasMaxLength(50);
            builder.Property(p => p.ServiceInfo).HasMaxLength(250);
            builder.HasOne(pc => pc.PartnerManager)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(pc => pc.PartnerManagerId);
            builder.HasOne<Preference>(pc => pc.Preference);
                // .WithMany(p => p.PromoCodes)
                // .HasForeignKey(pc=>pc.PreferenceId);
            builder.HasOne(pc => pc.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(pc => pc.CustomerId);
        }
    }
}
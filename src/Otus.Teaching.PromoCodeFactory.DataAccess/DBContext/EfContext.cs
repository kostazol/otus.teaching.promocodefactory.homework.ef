﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfConfigurations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public EfContext(DbContextOptions<EfContext> dbContextOptions) : base(dbContextOptions)
        {
            // Database.EnsureDeleted();
            // Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerPreferenceConfiguration());

            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор"
            });
            
            modelBuilder.Entity<Preference>().HasData(new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            });
            
            modelBuilder.Entity<Employee>().HasData(new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
            });
            
            modelBuilder.Entity<Customer>().HasData(new Customer()
            {
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
                Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
            });

            modelBuilder.Entity<PromoCode>().HasData(new PromoCode()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Code = "test123",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(5),
                PartnerName = "TestPartner",
                ServiceInfo = "PromoCode for sale",
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PartnerManagerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
            });
            
            // modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            // modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            // modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            // modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }
    }
}
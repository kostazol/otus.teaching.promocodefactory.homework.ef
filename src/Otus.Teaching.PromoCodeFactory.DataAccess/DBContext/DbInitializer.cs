﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class DropAndCreateDbInitializer
        : IDbInitializer
    {
        private readonly EfContext _efContext;

        public DropAndCreateDbInitializer(EfContext efContext)
        {
            _efContext = efContext;
        }
        
        public void Init()
        {
            _efContext.Database.EnsureDeleted();
            _efContext.Database.Migrate();
        }
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class testFieldToCustomerAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "testField",
                table: "Customers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2020, 6, 27, 3, 17, 2, 685, DateTimeKind.Local).AddTicks(9480), new DateTime(2020, 7, 2, 3, 17, 2, 686, DateTimeKind.Local).AddTicks(6778) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "testField",
                table: "Customers");

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2020, 6, 27, 3, 14, 54, 167, DateTimeKind.Local).AddTicks(7525), new DateTime(2020, 7, 2, 3, 14, 54, 168, DateTimeKind.Local).AddTicks(5015) });
        }
    }
}
